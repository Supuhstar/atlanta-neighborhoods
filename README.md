# Atlanta Neighborhoods #

I wanted to teach myself Atlanta and its neighborhoods and paths, and what better way than to make a map, myself?

License: [BH-1-PS](./LICENSE.txt)

---

## Preview: ##
![](./Atlanta%20neighborhoods%20map.svg)
